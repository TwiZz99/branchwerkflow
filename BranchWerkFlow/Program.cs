﻿using System;

namespace BranchWerkFlow
{
    class MainClass
    {
        public static void Main(string[] args)
        {

            int i, n, sum=0;



            Console.Write("Please enter a number : ");
            n = Convert.ToInt32(Console.ReadLine());
            Console.Write("\nThe first {0} natural number are :\n", n);
            for (i = 1; i <= n; i++)
            {
                Console.Write("{0} ", i);
                sum += i;

            }
            Console.Write("\nThe sum is : {1} \n", n,sum);

        }
    }
}
